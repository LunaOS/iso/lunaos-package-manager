all: install

install:
	for i in po/*.po; do lang="$$(basename "$$i" .po)" && mkdir -p $(DESTDIR)/usr/share/locale/"$$lang"/LC_MESSAGES/ && msgfmt -o $(DESTDIR)/usr/share/locale/"$$lang"/LC_MESSAGES/lpm.mo po/"$$lang".po ; done
	install -Dm755 "lpm" "$(DESTDIR)$(PREFIX)/bin/lpm"
	install -Dm644 "completions/bash" "$(DESTDIR)$(PREFIX)/share/bash-completion/completions/lpm";
pot:
	xgettext --keyword=prompt --keyword=msg -o po/lpm.pot --language Shell --from-code=utf-8 lpm
	for i in po/*.po; do msgmerge --update "$$i" "po/lpm.pot" ; done
	for i in po/*.po~; do rm "$$i" ; done
